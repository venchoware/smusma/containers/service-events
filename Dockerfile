# Build the executable.
FROM golang:alpine AS builder
LABEL maintainer="michael.sullivan@venchoware.com"

# Create appuser, an unpriviledged user.
ENV USER=appuser
ENV UID=10001
# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/app" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# Install git.
RUN apk update && apk add --no-cache git

# Build the app.
WORKDIR $GOPATH/src
RUN git clone https://gitlab.com/venchoware/smusma/events.git
WORKDIR $GOPATH/src/events
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o /app/events .

# Now build a small image.
FROM scratch

# Import the user and group files from the builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy the executable from the builder.
COPY --from=builder /app/events /app/events
EXPOSE 9091

# Use the unprivileged user.
USER appuser:appuser

# Run the executable
ENTRYPOINT [ "/app/events", "--port", "9091", \
	"--mq_service", "amqp://guest:guest@localhost:9090" ]
